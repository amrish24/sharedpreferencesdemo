package com.amrish.sharedprefdemo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText name, email;
    Button save, show, clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.editText);
        email = (EditText) findViewById(R.id.editText2);
        save = (Button) findViewById(R.id.button3);
        show = (Button) findViewById(R.id.button2);
        clear = (Button) findViewById(R.id.button);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getSharedPreferences("Details", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("Name", name.getText().toString());
                editor.putString("Email", email.getText().toString());
                editor.apply();

                Toast.makeText(getApplicationContext(),"Data is saved", Toast.LENGTH_SHORT).show();
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getSharedPreferences("Details", MODE_PRIVATE);
                String name1 = preferences.getString("Name", "");
                String email1 = preferences.getString("Email", "");
                name.setText(name1);
                email.setText(email1);
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setText("");
                email.setText("");
            }
        });
    }
}
